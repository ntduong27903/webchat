from fastapi import FastAPI, Request
from pydantic import BaseModel
from fastapi.middleware.cors  import CORSMiddleware
import mysql.connector

app = FastAPI()

mydb = mysql.connector.connect(
        host="127.0.0.1",
        user="admin",
        password="chemgio123",
        database="webchat",
        auth_plugin='caching_sha2_password'
    )

def write_in_database( message ):
    mycursor = mydb.cursor()

    sql = "INSERT INTO `webchat`.`message` (`msg`) VALUES (%s)"
    val = (message,)
    mycursor.execute(sql,val)
    
    mydb.commit()

def query_in_database():

    mycursor = mydb.cursor()

    sql = "SELECT * FROM webchat.message;"
    
    mycursor.execute(sql)

    mylist=[]
    for x in mycursor:
        mylist.append(x[1])
    return mylist

origins = [
    "http://127.0.0.1:8000",
    "http://production.social",
    "null"
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"]
)

class Item(BaseModel):
    msg : str

@app.get("/get-msg")
async def get_messages( request: Request):
    result = query_in_database()
    return result

@app.post("/send-msg")
async def send_to( request: Request):
    mess = await request.json()
    print(mess['msg'])
    write_in_database(mess['msg'])

